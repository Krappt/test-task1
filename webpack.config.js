var path = require('path');
var webpack = require('webpack');

module.exports = {
    entry: [
        'webpack-hot-middleware/client',
        './src/index'
    ],
    output: {
        path: path.join(__dirname, 'out'),
        filename: 'bundle.js',
        publicPath: '/out/'
    },
    cache: true,
    devtool: 'inline-source-map',
    module: {
        loaders: [
            {
                test: /\.scss/,
                include: path.join(__dirname, './src/assets/style'),
                loaders: ["style", "css", "sass"]
            },
            {
                loaders: ['react-hot', 'babel-loader'], //добавили loader 'react-hot'
                include: [
                    path.resolve(__dirname, "src")
                ],
                test: /\.js$/,
                plugins: ['transform-runtime']
            },
            {
                loaders: ['react-hot', 'babel-loader'], //добавили loader 'react-hot'
                include: [
                    path.resolve(__dirname, "src")
                ],
                test: /\.jsx$/,
                plugins: ['transform-runtime']
            }
        ]
    },
    resolve: {
        extensions: ['', '.js', '.jsx', ".scss"]
    }
    ,
    plugins: [
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoErrorsPlugin()
    ]
};