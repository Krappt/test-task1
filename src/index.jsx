import ReactDOM from 'react-dom';
import React from 'react';
import 'babel-polyfill';
import { Provider } from 'react-redux'
import injectTapEventPlugin from 'react-tap-event-plugin';
import Main from './containers/Main';
import configureStore from './store/configureStore'
import './assets/style/style';

const store = configureStore();

injectTapEventPlugin();

ReactDOM.render(
    <Provider store={store}>
        <Main/>
    </Provider>,
    document.querySelector('.root')
);  