import {
    CHANGE_POPUP,
    OPEN_POPUP,
    CLOSE_POPUP,
    OVER_POPUP,
    TOGGLE_ALL
} from '../constants/Statistics'

const initialState = {
    openedPopUps: [],
    currentPopUp: 0,
    opened: false,
    checkBox: false,
    overPopUp: 0,
    popupList: [
        {title: "Pop up 1",
        paragraphs: [
            "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.",
            "Et harum quidem rerum facilis est et expedita distinctio."
        ]},
        {title: "Pop up 2",
            paragraphs: [
                "On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain.",
                "These cases are perfectly simple and easy to distinguish.",
                "In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided.",
                "But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted.",
                "The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains."
            ]}
    ]
};

export default function statistics(state = initialState, action) {
    switch (action.type) {
        case TOGGLE_ALL:
            return { ...state, checkBox: !state.checkBox };
        case CHANGE_POPUP:
            return { ...state, currentPopUp: +action.payload };
        case OVER_POPUP:

            return { ...state, overPopUp: action.payload };
        case OPEN_POPUP:
            let popUps = [];
            let over = 0;

            if(!state.checkBox) {
                popUps.push(state.currentPopUp);
            }
            else {
                state.popupList.map(
                    (item,i) => {
                        popUps.push(i);
                        over = i;
                    }
                );
            }

            return { ...state, opened: true, openedPopUps: popUps, overPopUp: over };
        case CLOSE_POPUP:
            let index = action.payload;
            let popUpList = [];
            let opened = state.opened;

            if(index != "all") {
                state.openedPopUps.map(
                    (item,i) => {
                        if(i != index) {
                            popUpList.push(item);
                        }
                    }
                );
            }


            if(!popUpList.length) opened = false;

            return { ...state, opened: opened, openedPopUps: popUpList };
        default:
            return state;
    }

}