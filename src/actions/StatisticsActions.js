import {
    CHANGE_POPUP,
    OPEN_POPUP,
    CLOSE_POPUP,
    OVER_POPUP,
    TOGGLE_ALL
} from '../constants/Statistics'

export function toggleAll(event) {
    return {
        type: TOGGLE_ALL
    }
}

export function openPopup() {
    return {
        type: OPEN_POPUP
    }
}

export function closePopup(index) {
    return {
        type: CLOSE_POPUP,
        payload: index
    }
}

export function overPopup(index) {
    return {
        type: OVER_POPUP,
        payload: index
    }
}

export function changePopUp(params) {
    return {
        type: CHANGE_POPUP,
        payload: params
    }
}
