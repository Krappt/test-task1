import React, { PropTypes, Component } from 'react'
import Button from './Button';

export default class PopUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false,
            fade: 0
        };
    }

    static propTypes = {
        popUp: PropTypes.object.isRequired,
        close: PropTypes.func.isRequired,
        setOver: PropTypes.func.isRequired,
        shift: PropTypes.number.isRequired,
        index: PropTypes.number.isRequired,
        over: PropTypes.number.isRequired
    };

    componentWillMount() {
        let timeout = setTimeout(()=> {
            let interval = setInterval(() => {
                this.setState({fade: this.state.fade+=0.1});
                if(this.state.fade >= 1) {
                    clearInterval(interval);
                }
            }, 20);
            clearTimeout(timeout);
        }, this.props.shift*3);
    }

    close() {
        this.props.close(this.props.index);
    }

    over() {
        this.props.setOver(this.props.index);
    }

    render() {
        const {popUp, shift, index, over} = this.props;
        return (
            <div className="popup" onClick={::this.over} style={{left: `${shift}px`, top: `${shift}px`, zIndex: (over == index)?101:100, opacity: this.state.fade}}>
                <div className="popup-close" onClick={::this.close}/>
                <h1 className="popup-title">{popUp.title}</h1>
                <div className="popup-content">
                    {popUp.paragraphs.map(
                        (item,i) => <p key={i}>{item}</p>
                    )}
                </div>
                <div className="popup-bottom">
                    <Button title="Finish" action={::this.close}/>
                </div>
            </div>
        )
    }
}