import React, { PropTypes, Component } from 'react'

export default class DropDown extends Component {
    constructor(props) {
        super(props);
        this.state = {
            opened: false,
            clickActions: this.outerClick.bind(this)
        };
    }

    static propTypes = {
        popupList: PropTypes.array.isRequired,
        currentPopUp: PropTypes.number.isRequired,
        changePopUp: PropTypes.func.isRequired
    };

    componentDidMount() {
        document.addEventListener("click", this.state.clickActions);
    }

    componentWillUnmount() {
        document.removeEventListener("click", this.state.clickActions);
    }

    outerClick(event) {
        let target = event.target;
        let elem;

        if(target.className.indexOf("dropdown-head") >= 0) elem = target;
        else if(target.parentNode && target.parentNode.className && target.parentNode.className.indexOf("dropdown-head") >= 0) elem = target.parentNode;

        if(!elem && this.state.opened) this.toggle();
    }

    onClick() {
        this.toggle();
    }

    onSelect(event) {
        let item = event.target.getAttribute("data-item");
        this.props.changePopUp(item);
        this.toggle();
    }

    toggle() {
        this.setState({"opened": !this.state.opened});
    }

    render() {
        const {currentPopUp, popupList} = this.props;

        return (
            <div className={(this.state.opened)? "dropdown opened" : "dropdown"}>
                <div className="dropdown-head" onClick={::this.onClick}>
                    <span className="dropdown-title">{popupList[currentPopUp].title}</span>
                    <span className="dropdown-arrow"/>
                </div>
                <div className="dropdown-list">
                    {popupList.map(
                        (item,i) => <div className="dropdown-item" key={i} onClick={::this.onSelect} data-item={i}>{item.title}</div>
                    )}
                </div>
            </div>
        )
    }
}