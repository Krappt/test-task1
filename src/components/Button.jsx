import React, { PropTypes, Component } from 'react'

export default class Button extends Component {
    constructor(props) {
        super(props);
    }

    static propTypes = {
        title: PropTypes.string.isRequired,
        action: PropTypes.func.isRequired
    };

    onClick() {
        this.props.action();
    }

    render() {
        const {title} = this.props;

        return (<div className="button" onClick={::this.onClick}>{title}</div>)
    }
}