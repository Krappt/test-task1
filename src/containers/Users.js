import React, { PropTypes, Component } from 'react'
import { connect } from 'react-redux';

export class Users extends Component {
    constructor() {
        super();
    }


    render() {
        return (
            <div className="tabs-inner">
                <div className="tabs-body">
                    <h1 className="title">Some users</h1>
                </div>
            </div>
        )
    }
}


function mapStateToProps (state) {
    return {

    }
}

function mapDispatchToProps(dispatch) {
    return {

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Users)