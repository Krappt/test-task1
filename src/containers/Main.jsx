import React, { PropTypes, Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux';
import Statistics from './Statistics';
import Users from './Users';
import Tabs from 'react-simpletabs';
import PopUp from './../components/PopUp';
import * as StatisticsActions from '../actions/StatisticsActions'

export class Main extends Component {
    constructor() {
        super();
    }


    closePopUp() {
        this.props.statisticsActions.closePopup("all");
    }

    render() {
        const { statistics } = this.props;
        const { closePopup, overPopup } = this.props.statisticsActions;

        return (
            <div>
                <Tabs tabActive={2}>
                    <Tabs.Panel title='Users'>
                        <Users/>
                    </Tabs.Panel>
                    <Tabs.Panel title='Statistics'>
                        <Statistics/>
                    </Tabs.Panel>
                </Tabs>
                <div>
                    {(() => {
                        if (statistics.opened) {
                            return <div className="dark-bg" onClick={::this.closePopUp}></div>
                        }
                    })()}
                    {(() => {
                        if (statistics.opened) {
                            return (
                                statistics.openedPopUps.map(
                                    (item,i) => {
                                        return <PopUp popUp={statistics.popupList[item]} close={closePopup} setOver={overPopup} over={statistics.overPopUp} shift={i * 100} index={i} key={i}/>
                                    }
                                )
                            )
                        }
                    })()}
                </div>
            </div>
        )
    }
}


function mapStateToProps (state) {
    return {
        statistics: state.statistics,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        statisticsActions: bindActionCreators(StatisticsActions, dispatch),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Main)