import React, { PropTypes, Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux';
import * as StatisticsActions from '../actions/StatisticsActions'
import DropDown from './../components/DropDown';
import Button from './../components/Button';

export class Statistics extends Component {
    constructor() {
        super();
    }

    render() {
        const { statistics } = this.props;
        const { changePopUp, toggleAll, openPopup } = this.props.statisticsActions;

        return (
            <div className="tabs-inner">
                <div className="tabs-body">
                    <h1 className="title">Some statistics</h1>
                    <div className="selector">
                        <p>
                            <input type="checkbox" id="all" onChange={toggleAll} value={statistics.checkBox ?'on' : 'off'}/>
                            <label htmlFor="all"></label>
                            <span>Open all popups</span>
                        </p>
                    </div>
                    <div className="bottom">
                        <DropDown popupList={statistics.popupList} currentPopUp={statistics.currentPopUp} changePopUp={changePopUp}/>
                        <div className="bottom-right">
                            <Button title="Open" action={openPopup}/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


function mapStateToProps (state) {
    return {
        statistics: state.statistics,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        statisticsActions: bindActionCreators(StatisticsActions, dispatch),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Statistics)