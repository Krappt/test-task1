export const CHANGE_POPUP = 'CHANGE_POPUP';
export const TOGGLE_ALL = 'ENABLE_ALL';
export const OPEN_POPUP = 'OPEN_POPUP';
export const CLOSE_POPUP = 'CLOSE_POPUP';
export const OVER_POPUP = 'OVER_POPUP';
